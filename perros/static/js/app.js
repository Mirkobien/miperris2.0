var chile= 
{
    "regiones": [{
        "region": "Arica y Parinacota",
        "comunas": ["Arica", "Camarones", "Putre", "General Lagos"]
    },
    {
        "region": "Tarapacá",
        "comunas": ["Iquique", "Alto Hospicio", "Pozo Almonte", "Camiña", "Colchane", "Huara", "Pica"]
    },
    {
        "region": "Antofagasta",
        "comunas": ["Antofagasta", "Mejillones", "Sierra Gorda", "Taltal", "Calama", "Ollagüe", "San Pedro de Atacama", "Tocopilla", "María Elena"]
    },
    {
        "region": "Atacama",
        "comunas": ["Copiapó", "Caldera", "Tierra Amarilla", "Chañaral", "Diego de Almagro", "Vallenar", "Alto del Carmen", "Freirina", "Huasco"]
    },
    {
        "region": "Coquimbo",
        "comunas": ["La Serena", "Coquimbo", "Andacollo", "La Higuera", "Paiguano", "Vicuña", "Illapel", "Canela", "Los Vilos", "Salamanca", "Ovalle", "Combarbalá", "Monte Patria", "Punitaqui", "Río Hurtado"]
    },
    {
        "region": "Valparaíso",
        "comunas": ["Valparaíso", "Casablanca", "Concón", "Juan Fernández", "Puchuncaví", "Quintero", "Viña del Mar", "Isla de Pascua", "Los Andes", "Calle Larga", "Rinconada", "San Esteban", "La Ligua", "Cabildo", "Papudo", "Petorca", "Zapallar", "Quillota", "Calera", "Hijuelas", "La Cruz", "Nogales", "San Antonio", "Algarrobo", "Cartagena", "El Quisco", "El Tabo", "Santo Domingo", "San Felipe", "Catemu", "Llaillay", "Panquehue", "Putaendo", "Santa María", "Quilpué", "Limache", "Olmué", "Villa Alemana"]
    },
    {
        "region": "Región del Libertador Gral. Bernardo O’Higgins",
        "comunas": ["Rancagua", "Codegua", "Coinco", "Coltauco", "Doñihue", "Graneros", "Las Cabras", "Machalí", "Malloa", "Mostazal", "Olivar", "Peumo", "Pichidegua", "Quinta de Tilcoco", "Rengo", "Requínoa", "San Vicente", "Pichilemu", "La Estrella", "Litueche", "Marchihue", "Navidad", "Paredones", "San Fernando", "Chépica", "Chimbarongo", "Lolol", "Nancagua", "Palmilla", "Peralillo", "Placilla", "Pumanque", "Santa Cruz"]
    },
    {
        "region": "Región del Maule",
        "comunas": ["Talca", "Constitución", "Curepto", "Empedrado", "Maule", "Pelarco", "Pencahue", "Río Claro", "San Clemente", "San Rafael", "Cauquenes", "Chanco", "Pelluhue", "Curicó", "Hualañé", "Licantén", "Molina", "Rauco", "Romeral", "Sagrada Familia", "Teno", "Vichuquén", "Linares", "Colbún", "Longaví", "Parral", "Retiro", "San Javier", "Villa Alegre", "Yerbas Buenas"]
    },
    {
        "region": "Región de Ñuble",
        "comunas": ["Cobquecura", "Coelemu", "Ninhue", "Portezuelo", "Quirihue", "Ránquil", "Treguaco", "Bulnes", "Chillán Viejo", "Chillán", "El Carmen", "Pemuco", "Pinto", "Quillón", "San Ignacio", "Yungay", "Coihueco", "Ñiquén", "San Carlos", "San Fabián", "San Nicolás"]
    },
    {
        "region": "Región del Biobío",
        "comunas": ["Concepción", "Coronel", "Chiguayante", "Florida", "Hualqui", "Lota", "Penco", "San Pedro de la Paz", "Santa Juana", "Talcahuano", "Tomé", "Hualpén", "Lebu", "Arauco", "Cañete", "Contulmo", "Curanilahue", "Los Álamos", "Tirúa", "Los Ángeles", "Antuco", "Cabrero", "Laja", "Mulchén", "Nacimiento", "Negrete", "Quilaco", "Quilleco", "San Rosendo", "Santa Bárbara", "Tucapel", "Yumbel", "Alto Biobío"]
    },
    {
        "region": "Región de la Araucanía",
        "comunas": ["Temuco", "Carahue", "Cunco", "Curarrehue", "Freire", "Galvarino", "Gorbea", "Lautaro", "Loncoche", "Melipeuco", "Nueva Imperial", "Padre las Casas", "Perquenco", "Pitrufquén", "Pucón", "Saavedra", "Teodoro Schmidt", "Toltén", "Vilcún", "Villarrica", "Cholchol", "Angol", "Collipulli", "Curacautín", "Ercilla", "Lonquimay", "Los Sauces", "Lumaco", "Purén", "Renaico", "Traiguén", "Victoria"]
    },
    {
        "region": "Región de Los Ríos",
        "comunas": ["Valdivia", "Corral", "Lanco", "Los Lagos", "Máfil", "Mariquina", "Paillaco", "Panguipulli", "La Unión", "Futrono", "Lago Ranco", "Río Bueno"]
    },
    {
        "region": "Región de Los Lagos",
        "comunas": ["Puerto Montt", "Calbuco", "Cochamó", "Fresia", "Frutillar", "Los Muermos", "Llanquihue", "Maullín", "Puerto Varas", "Castro", "Ancud", "Chonchi", "Curaco de Vélez", "Dalcahue", "Puqueldón", "Queilén", "Quellón", "Quemchi", "Quinchao", "Osorno", "Puerto Octay", "Purranque", "Puyehue", "Río Negro", "San Juan de la Costa", "San Pablo", "Chaitén", "Futaleufú", "Hualaihué", "Palena"]
    },
    {
        "region": "Región Aisén del Gral. Carlos Ibáñez del Campo",
        "comunas": ["Coihaique", "Lago Verde", "Aisén", "Cisnes", "Guaitecas", "Cochrane", "O’Higgins", "Tortel", "Chile Chico", "Río Ibáñez"]
    },
    {
        "region": "Región de Magallanes y de la Antártica Chilena",
        "comunas": ["Punta Arenas", "Laguna Blanca", "Río Verde", "San Gregorio", "Cabo de Hornos (Ex Navarino)", "Antártica", "Porvenir", "Primavera", "Timaukel", "Natales", "Torres del Paine"]
    },
    {
        "region": "Región Metropolitana de Santiago",
        "comunas": ["Cerrillos", "Cerro Navia", "Conchalí", "El Bosque", "Estación Central", "Huechuraba", "Independencia", "La Cisterna", "La Florida", "La Granja", "La Pintana", "La Reina", "Las Condes", "Lo Barnechea", "Lo Espejo", "Lo Prado", "Macul", "Maipú", "Ñuñoa", "Pedro Aguirre Cerda", "Peñalolén", "Providencia", "Pudahuel", "Quilicura", "Quinta Normal", "Recoleta", "Renca", "San Joaquín", "San Miguel", "San Ramón", "Vitacura", "Puente Alto", "Pirque", "San José de Maipo", "Colina", "Lampa", "Tiltil", "San Bernardo", "Buin", "Calera de Tango", "Paine", "Melipilla", "Alhué", "Curacaví", "María Pinto", "San Pedro", "Talagante", "El Monte", "Isla de Maipo", "Padre Hurtado", "Peñaflor"]
        }]
    }
/*Abrir||Cerrar formulario clientes*/
$("#registro").click(function(){
    if ($("#division").is(':visible')){
        $("#division").hide(); 
    }else {
        $("#division").show();
    }
});

$(function(){
    //Al momento de cargar el DOM, las regiones estarán cargadas
    cargarRegion();
    //Al momento de cargar el DOM, el formulario aparecerá escondido 
    $("#division").hide();
})

function cargarRegion(){
    chile.regiones.forEach(region => {
        $("#region").append('<option value="'+region.region+'">'+region.region+'</option>')
    })
}

//Al momento de dar click sobre un elemento del checkbox cargar región, se cargará la comuna respectiva
$("#region").change(function(){
    var region = $(this).val()
    var regionCompleta = chile.regiones.find(r =>r.region == region)
    $("#comuna").html("")
    $("#comuna").append('<option hidden ">Seleccione</option>')
    regionCompleta.comunas.forEach(comuna => {
       $("#comuna").append('<option value="'+comuna+'">'+comuna+'</option>')
   })
    
})

//Validaciones 
function validaRut(rut){ 
    console.clear("")
    rut = rut.replace('-','') 
    rut = rut.replace('-','')
    rut = rut.replace('-','')
    rut = rut.replace('/','') 
    rut = rut.replace('/','')
    rut = rut.replace('/','')
    if (rut != '' && rut.length > 1) {
        var sinPuntos = rut.replace(/\./g, "");
        var actualLimpio = sinPuntos.replace(/-/g, "");
        console.log("Actual limpio: "+actualLimpio)
        var inicio = actualLimpio.substring(0, actualLimpio.length - 1);
        console.log("inicio: "+inicio)
        var rutPuntos = "";
        var i = 0;
        var j = 1;
        var suma = 0;
	    var caracteres = "1234567890kK";
        var contador = 0; 

        for (var i=0; i < actualLimpio.length; i++){
            u = actualLimpio.substring(i, i + 1);
            if (caracteres.indexOf(u) != -1)
            contador ++;
        }
        if ( contador==0 ) { return ''}

        for (i = inicio.length - 1; i >= 0; i--) { //inicio = 20129122
            var letra = inicio.charAt(i);
            rutPuntos = letra + rutPuntos;
            if (j % 3 == 0 && j <= inicio.length - 1) {
                rutPuntos = "." + rutPuntos;
            }
            j++;
        }
        var drut = actualLimpio.substring(actualLimpio.length - 1);
        rutPuntos = rutPuntos + "-" + drut;
        console.log('rut puntos: '+rutPuntos)
        console.log('Actual Limpio: '+actualLimpio)
        console.log('Digito Verificador: '+drut)
        var rut = actualLimpio.substring(0,actualLimpio.length-1)
        console.log('rut: '+rut)
        var dvr = '0';
        var mul = 2;

        //Modulo 11
        for (i= rut.length -1 ; i >= 0; i--) {
            suma = suma + rut.charAt(i) * mul
                    if (mul == 7) 	mul = 2
                    else	mul++
        }
        res = suma % 11
        if (res==1)		dvr = 'k'
                    else if (res==0) dvr = '0'
        else {
            dvi = 11-res
            dvr = dvi + ""
        }

        if ( dvr != drut.toLowerCase() ) { return ''; }
        else { return rutPuntos; }
    }
    return rutPuntos;   
}

function validaFecha(y,m,d){
    //si desde el año que nací, después de 18 años es menor o igual a la fecha actual mi edad, se cumple. m-1 porque si nací en el mes 8 el programa lo lee como mes 9
    return (new Date(y+18,m-1,d) <= new Date());
}

/*
function validaFecha(date){
    console.clear("")
    var fecha = date.split("-")
    var y = parseInt(fecha[0],10)
    fechaActual = new Date(); 
    console.log("Fecha actual: "+fechaActual.toString());
    if(y <= 2001){
        return true;
    }else{
        return false;
    }
}
*/

jQuery.validator.addMethod("rut", function(value, element) { 
    console.clear("")
    var tamanioRut = validaRut(value);
    console.log('tamaño rut: '+tamanioRut.length)
    console.log('value rut: '+value)
    if (tamanioRut.length == 12 || tamanioRut.length == 11){
        element.value=tamanioRut;
        return true}else{return false}
}, "Revise el RUT");


$.validator.addMethod("alpha", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
 }, "Revise el nombre");

 $.validator.addMethod("valDate", function(value, element) {
     console.clear("")
     fechaUsuario = value.split("-");
     y = parseInt(fechaUsuario[0],10);
     m = parseInt(fechaUsuario[1],10); 
     d = parseInt(fechaUsuario[2],10);
     var resultado = validaFecha(y,m,d);
    return this.optional(element) || resultado;
}, "Debe ser mayor de 18 años");

function miFuncion(event){
    console.clear("");
    let x = String.fromCharCode(event.keyCode);
    console.log(event.keyCode + " " + x);
    var cadena = "0123456789"; 
    console.log(cadena.indexOf(x))
    console.log($("#nombre").val())
    if (cadena.indexOf(x) != -1){
        event.preventDefault()
        $("#nombre").val($("#nombre").val().replace(x,""));
    }

}

$(function(){
    var FillField= "Se requiere completar este campo"
        $("#formulario").validate({
            lang: 'es',
            rules:{
                correo:{
                    required:true, 
                    email:true
                },
                run:{
                    required:true, 
                    rut:true //La funcion se llama rut
                },
                nombre:{
                    required:true,
                    alpha:true,
                }
                ,
                numero:{
                    required:true,
                }, 
                fecha:{
                    required:true,
                    valDate:true,
                }
            }
            ,
            messages: {
                correo:{
                    required:FillField,
                    email:"Ingrese un email válido"
                },
                run:{
                    required:FillField, 
                    rut:"¡Formato Incorrecto!"
                },
                nombre:{
                    required:FillField, 
                    alpha:"¡Solo se aceptan caracteres no numéricos!"
                }
                ,
                numero:{
                    required:FillField, 
                },
                fecha:{
                    required:FillField,
                }
            }
        })
})

$(function(){
    var formulario = document.getElementById('formulario')[0], 
    elementos = formulario.elements, 
    boton = document.getElementById('enviar');

    var validarNombre = function(){
        if(formulario.nombre.val() == 0){
            alert("Completa el campo nombre");
        }
    }

    var validar = function(){
        validarNombre();
    }

    console.log(formulario.addEventListener("submit",validar)) ;

});



/*Modal */
$(function() {
    $('.aparecer').on('click', function(e) {
        $('.imagepreview').attr('src', $(this).find('img').attr('src'));
        $('#texto-bajo').html('');
        $('#nombrePerro').html('');
        $('#texto-bajo').html($(this).find('img').attr('data-desc'));
        $('#nombrePerro').html($(this).find('img').attr('data-nombre'));
        $('#imagemodal').modal('show'); 
        e.preventDefault();
    });		
});
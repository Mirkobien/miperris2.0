from . import views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from rest_framework import routers
from django.conf.urls import url, include
from django.views.generic import TemplateView

router = routers.DefaultRouter()
router.register(r'personas', views.PersonaViewSet)
router.register(r'perros', views.PerroViewSet)

urlpatterns = [
    path('',views.index,name="index"),
    path('registrar_perro',views.registrarPerro,name="registrar_perro"),
    path('registrar_usuario',views.registrarUsuario,name="registrar_usuario"),
    path('login',views.login_page,name="login_page"),
    path('logining',views.login,name="login"),
    path('cerrar_sesion',views.cerrar_sesion,name="cerrar_sesion"),
    path('administrar',views.administrar,name="administrar"),
    path('administrar/borrar_perro/<int:id>',views.borrar_perro,name="borrar_perro"),
    path('administrar/editar_perro',views.editar_perro,name="editar_perro"),  
    path('auth/', include('social_django.urls', namespace='social')),
    url(r'^sw.js', (TemplateView.as_view(template_name="sw.js", content_type='application/javascript', )), name='sw.js'),
    url(r'^manifest.json', (TemplateView.as_view(template_name="manifest.json", content_type='application/json', )), name='manifest.json'),
    path('api/', include(router.urls)),
] +static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
from .models import Persona,Perro
from rest_framework import serializers


class PerroSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Perro
        fields = ('url', 'nombre', 'raza', 'descripcion', 'estado', 'foto')


class PersonaSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Persona
        fields = ('url', 'nombre', 'correo', 'run', 'fecha_nacimiento', 'numero', 'region', 'ciudad', 'tipo_vivienda')